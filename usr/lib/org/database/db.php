<?php
namespace usr\lib;


class db
{
    public function __construct()
    {
        global $system;
        $ePath = "usr/etc";  // 公共配置目录
        $mePath = 'usr/'.$system->config->module . "/{$system->config->model}";  // 私有配置目录
        $file = $mePath. "/dbs.php";
        if (file_exists($file)) {
            require_once($file);
        }
        else {
            $file = $ePath. "/dbs.php";
            require_once($file);
        }
        foreach ($db as $name => $info) {
            $this->$name = $info;
        }
    }

    public function load($name)
    {
        $info = $this->$name;
    }
}