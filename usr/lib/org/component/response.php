<?php
namespace usr\lib;


class response
{
    private $respGet;
    private $respPost;

    public function __construct()
    {
        $this->respGet = &$_GET;
        $this->respPost = &$_POST;
    }

    public function get($key)
    {
        if (isset($this->respGet[$key])) {
            return $this->respGet[$key];
        }
        return false;
    }

    public function post($key)
    {
        if (isset($this->respPost[$key])) {
            return $this->respPost[$key];
        }
        return false;
    }

    public function __get($key)
    {
        if ($key === 'get') {
            return $this->respGet;
        }
        elseif ($key == 'post') {
            return $this->respPost;
        }
        else {
            die("仅支持 get 和 post");
        }
    }
}