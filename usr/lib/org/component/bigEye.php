<?php
namespace usr\lib;
use bin\dataParse;
use usr\lib\moduleBase;
global $use;
$use('component', 'moduleBase');


class bigEye extends moduleBase
{
    private $pass = true;  // 默认不使用过滤器可通过

    public function __construct()
    {
        parent::__construct();
    }

/**
 * @param filter 滤镜类名称
 * @param param run 方法参数
 */
    public function filter(array $filter, array $param=[])
    {
        foreach ($filter as $once) {
            $space = "\\usr\\module\\filter\\";
            $space = $space . $once;  // 类全称
            $path  = $this->path . "/filter/$once.php";  // filter 路径
            global $use;
            require_once($path);  // 加载文件
            $cl = new $space;
            $cl->param = $param;
            $result = $cl->run();
            if (! $result) {
                $cl->fail();
                $this->pass = false;  // 声明 '禁止通过'
                return false;
            }
            $cl->success();
        }
        return true;
    }

    public function live()
    {
        // 未通过则记录访问者信息到 log 目录
        if (! $this->pass) {
            echo $_SERVER["REMOTE_ADDR"];
        }
    }

/**
 * @param bin 要执行的类名称(文件名与类名一致)
 * @param param 参数数组
 */
    public function to($bin, array $param=[])
    {
        $binPath = $this->path . "/bin/$bin.php";
        $binName = "\\usr\\module\\bin\\$bin";
        global $use;  // 传递 use 函数
        require_once($binPath);
        return call_user_func_array([
            $cl = new $binName(),
            'init'
        ], $param);
    }

}