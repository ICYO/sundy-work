<?php
namespace usr\lib;
use usr\lib\moduleBase;
global $use;
$use('component', 'moduleBase');


class easyBin extends moduleBase
{
    public function __construct()
    {
        parent::__construct();
        $this->out = $this->_out();  // 加载 out 类
    }

    public function _out()
    {
        return $this->using->lib('out');
    }
}