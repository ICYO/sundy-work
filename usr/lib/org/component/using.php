<?php
namespace usr\lib;


class using
{
    public function __construct()
    {
        global $system;
        $this->config = $system->config;
        $this->url = "usr/{$this->config->module}/{$this->config->model}";
        $this->lib = $this->url .'/lib';
        $this->etc = $this->url .'/etc';
        $this->log = $this->url .'/log';
        $this->db = $this->url .'/db';
    }

    public function lib($name)
    {
        $path = $this->lib . "/$name.php";  // 模块层
        $space = "\\usr\\module\\lib\\$name";  // 命名空间
        if (! file_exists($path)) {
            $path = "usr/lib/{$this->config->lib}/component/$name.php";  // 当前库
            if (! file_exists($path)) {
                $path = "usr/lib/org/component/$name.php";  // 默认库
                if (! file_exists($path)) {
                    header('HTTP/1.1 500 Internal Server Error', true, 500);
                    die('没有找到此函数库: '. $name);
                }
            }
            $space = "\\usr\\lib\\$name";  // 命名空间
        }
        global $use;  // 加载 use 方法
        require_once($path);
        return new $space();
    }

    public function db($name)
    {
        $path = $this->db . "/$name.php";  // 模块层
        $space = "\\usr\\module\\db\\$name";  // 命名空间
        if (! file_exists($path)) {
            header('HTTP/1.1 500 Internal Server Error', true, 500);
            die('没有找到此数据模型: '. $name);
        }
        global $use;  // 加载 use 方法
        require_once($path);  // 只从用户模块加载
        return new $space();
    }
}