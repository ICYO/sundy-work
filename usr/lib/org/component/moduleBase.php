<?php
namespace usr\lib;


abstract class moduleBase
{
    public function __construct()
    {
        global $system;  // 系统层环境
        global $module;  // 模块层环境
        $this->module = $module;
        $this->config = $system->config;
        $this->path = $module->path;  // 模块相对路径
        $this->session = &$_SESSION;
        $this->cookie = &$_COOKIE;
        $this->response = $this->_response();  // 衔接 response 类
        $this->in = $this->response;  // 简写 response 对象
        $this->using = $this->_using();
    }

/**
 * 模块层各文件之间可以使用 using 类相互调用
 */
    private function _using()
    {
        $path = "usr/lib/{$this->config->lib}/component/using.php";
        if (! file_exists($path)) {
            $path = "usr/lib/org/component/using.php";
        }
        $space = '\\usr\\lib\\using';
        require_once($path);
        return new $space();
    }

    private function _response()
    {
        $path = "usr/lib/{$this->config->lib}/component/response.php";
        if (! file_exists($path)) {
            $path = "usr/lib/org/component/response.php";
        }
        $space = '\\usr\\lib\\response';
        require_once($path);
        return new $space();
    }
}