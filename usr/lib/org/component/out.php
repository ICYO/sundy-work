<?php
namespace usr\lib;
use bin\dataParse;


class out
{
    public function __construct()
    {
        global $system;  // 系统层环境
        global $module;  // 模块层环境
        $this->module = $module;
        $this->config = $system->config;
        $this->path = $module->path;  // 模块相对路径
        $this->theme = isset($module->theme) ? $module->theme : $system->config->theme;  // 主题包
    }

    public function json($js)
    {
        header("Content-type: application/json");
        if (is_array($js)) {
            echo json_encode($js);
            return true;
        }
        if (file_exists($js)) {
            $tplt = $this->path . "/template";
            $js = $tplt . "/$js";
            echo file_get_contents($js);
            return true;
        }
        echo $js;
        return true;
    }

    public function html($html)
    {
        $tplt = $this->path . "/template";
        $html = $tplt . "/$html";
        ob_start();
        include($html);
        $out = ob_get_contents();
        // $out = $this->_rewrite($out);
        ob_end_clean();
        header("Content-type: text/html");
        echo $out;
    }

    private function _rewrite($out)
    {
        $out = preg_replace('/\{\{/', '<?php', $out);
        $out = preg_replace('/\}\}/', '?>', $out);
        return $out;
    }
}