<?php
namespace usr\lib;
use bin\dataParse;


class modelSystem
{
    public $path;

    public function __construct(array $_api)
    {
        $this->api = $_api;  // 注册api数据
    }

    public function loadModel()
    {
        $system = $this;  // 命名系统
        require($this->path. '/index.php');  // 引入模块入口脚本
    }

    public function init($xml)
    {
        $parse = new dataParse();  // 连接数据解析类
        $envXml = $this->path . "/$xml";
        $data = $parse->xml_object($envXml);
        $this->makeVal($data);
    }

    private function makeVal($data)
    {
        foreach ($data as $key => $val) {
            $val = trim($val);
            $this->$key = $val;
        }
    }

    public function turn($sc)
    {
        if ($sc == 'off') {
            return false;
        }
        elseif ($sc == 'on'){}
        else {
            die('参数错误');
        }
        global $use;  // 开启全局 use
        global $module;  // module 环境变量转为全局
        $use = $this->_useLib();  // 引入 use 闭包

        $_method = $this->api['event'];  // 定位event方法
        $_url = $this->path . "/event.php";  // 定位文件
        $_app = "\\usr\\module\\event";  // 定位 event 类
        $module = $this;
        require_once($_url);  // 引入文件
        call_user_func_array(  // 调用指定方法
            array(
                new $_app,  // 类
                $_method  // 方法
            ),
            $this->api['param']  // 参数
        );
    }

    public function _useLib()
    {
        $system = $this;
        return function ($pack, $lib) {
            $url = $this->lib;
            $libPath = "usr/lib/{$this->lib}/$pack/$lib.php";
            if (! file_exists($libPath)) {
                $libPath = "usr/lib/org/$pack/$lib.php";
            }
            $space = "\\usr\\lib\\$lib";
            require_once($libPath);
        };
    }
}