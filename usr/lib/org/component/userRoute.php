<?php
namespace usr\lib;
/**
 * 二级路由
 * [0] model 模块索引, 负责定位到具体模块
 * [1] url   模块参数字段(二级URL片段)
 */


class userRoute
{
    private $model;
    private $url;

    public function loadConfig($config)
    {
        $this->config = $config;
    }

    public function send()
    {
        $module = $this->config->module;
        $model = $this->config->model;
        $this->path = "usr/$module/$model";  // 定位模块目录
        $this->checkPath($this->path);  // 对目录做验证
        $route = $this->makeRoute();  // 闭包路由传递 $route 函数
        $path = "{$this->path}/config/rule.php";  // 定位路由表地址
        include_once($path);  // 加载路由文件
    }

    private function checkPath($path)
    {
        if (is_dir($path)) {
            return true;
        }
        die(__FILE__. __LINE__);
    }

    private function makeRoute()
    {
        return function (array $rule) {
            $this->route($rule);
        };
    }

/**
 * @param rule 用户模块路由数组(定义在用户模块的config目录)
 * 最终需要终止执行 (return 或 exit)
 */
    private function route($rule)
    {
        foreach ($rule as $reg => $index) {
            $reg = $this->sugerRe($reg);  // 添加正则语法糖支持
            if (preg_match($reg, $this->config->url, $match)) {
                unset($match[0]);  // 去除无关数据
                if (in_array('', $match)) break;  // 截取的参数如果有空值则不匹配
                $this->param = (array)$match;  // 得到参数
                $this->run   = $index;  // 得到事件索引
                return true;
            }
        
        }
        header("HTTP/1.1 404 Not Found", true, 404);
        die("404 not found");
    }

    public function connectModel()
    {
        $api = [
            'event' => $this->run,
            'param' => $this->param
        ];
        $file = "usr/lib/{$this->config->lib}/component/modelSystem.php";
        if (! file_exists($file)) {
            $file = "usr/lib/org/component/modelSystem.php";
        }
        require_once($file);
        $system = new \usr\lib\modelSystem($api);  // 加载模块系统环境
        $system->path = $this->path;
        $system->lib = $this->config->lib;
        $system->loadModel();
    }

    private function sugerRe($reg)
    {
        $reg = preg_replace('/:any/', '.*?', $reg);  // “匹配所有” 语法糖
        $reg = preg_replace('/:num/', '[0-9]+', $reg);  // “匹配所有” 语法糖
        $reg = preg_replace('/\$/', '\/?$', $reg);  // 兼容闭合与开放的 url 格式
        return $reg;
    }
}
