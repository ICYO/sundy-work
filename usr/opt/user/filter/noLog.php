<?php
namespace usr\module\filter;
use usr\lib\filter;
$use('component' ,'filter');


class noLog extends filter
{
    public $param = [];

    public function __construct()
    {
        parent::__construct();
        echo "<font color='#4169E1'>noLog filter init</font><br />";
    }

/**
 * 执行具体的过滤
 * @return bool 布尔值
 */
    public function run()
    {
        return true;
    }

/**
 * 成功时会调用此方法
 * @return 任何
 */
    public function success()
    {
        
    }

/**
 * 失败时会调用此方法
 * @return 任何
 */
    public function fail() {
        echo 'some thing is wrong';
    }

}