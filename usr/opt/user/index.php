<?php
/**
 * 模块层入口
 * 从此脚本开始的相对路径均以此脚本所在的目录为根目录
 * 注意 config/rule.php 与本模块从此脚本开始的后续执行无关
 */

$system->init('config/env.xml');  // 载入环境变量为全局的 module 变量

$system->turn('on');  // 模块层开关 (on/off)
// echo $system->charset;
