<?php
use usr\bin\userSys;
global $system;
/**
 * 用户层入口, 待优化, 需要最大限度的减小系统层传来的数据, 但现在仍有部分冗余数据($system = $this)
 */


// 重置系统层环
// 初始化用户层环境
$system = new userSys();
$system->init('usr/etc/local.xml');
$system->loadApi($api);  // 加载 api 数据到 config
// 初始化完毕

$system->route(true);
// print_r($system->config);