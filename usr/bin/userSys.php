<?php
namespace usr\bin;
use bin\sys;


class userSys extends sys
{
    public function __construct()
    {
        parent::__construct();
    }

/**
 * @param api 从系统层来的对接字串
 */
    public function loadApi($api)
    {
        foreach ($api as $key => $val) {
            $this->config->add($key, $val);
        }
    }

    public function route($key)
    {
        if (! $key) {
            echo $this->config->url;
            return false;
        }
        $route = $this->using->lib('usr\\lib', 'userRoute');
        $route->loadConfig($this->config);  // 传入模块信息
        $route->send();  // 发送请求到具体模块模块
        $route->connectModel();
    }
}