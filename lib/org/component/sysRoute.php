<?php
namespace lib;
use bin\dataParse;
// 一级路由
/**
 * [0] 模块索引字段: 请求URL中, 紧跟入口脚本字段后的一节
 * [1] 一级路由片段: 去除入口脚本后的 URL 片段
 * [2] 二级路由片段: 去除模块索引字段后的一级路由片段
 */


class sysRoute
{
    public $file;  // 路由表文件
    public $urlInit;  // 入口脚本
    private $rule;  // 路由表对象
    private $model;  // 匹配的用户层模块(用于对接)
    private $model_req;  // 二级路由所需的字符串(用于对接)

    public function init()
    {
        $parse = new dataParse();  // 初始化 xml 解析工具
        $route = $parse->xml_object($this->file);  // route 对象
        $this->rule = $route;
        $this->check();
    }

    public function check()
    {
        $firstReg = preg_replace('/\//', '\\/', $this->urlInit);  // 转义斜杠为 \/
        $firstReg = "^\/{$firstReg}";  // 拼接正则表达式
        $forstUrl = preg_replace("/"."$firstReg".'(\/|$)/', '', $_SERVER['REQUEST_URI']);  // 截取一级路由片段

        $model = $this->getModel($forstUrl);
        if (! $model) {
            $model = trim($this->rule->default);
        }
        $this->model = $model;
        $this->makeApi();
    }

    private function makeApi()
    {
        $data = [
            'model' => $this->model,
            'url' => $this->model_req
        ];
        $this->api = $data;
    }


/**
 * 返回匹配的模块名称, 生成二级路由片段
 * @param url 一级路由片段
 * @return 匹配的模块名称或 (bool)false
 */
    private function getModel($url)
    {
        foreach ($this->rule as $key => $val) {
            $key = trim($key);
            $val = trim($val);
            $keyReg = "^$key(\/|$)";
            if (preg_match('/'.$keyReg.'/', $url)) {
                $this->model_req = preg_replace('/'."^$key".'(\/|$)/', '', $url);  // 生成二级路由片段
                return $val;
            }
        }
        return false;
    }
}