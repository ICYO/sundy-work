<?php
use \bin\sys;
require_once("bin/sys.php");
/**
 * lio's weekend
 * 入口脚本 index.php
 * 
 * @author  lio
 * @since	Version 1.0.0
 * @deprecated lio's weekend 实例.
 * @version 1.0.0
 * @link http://git.oschina.net/icyo/php_wiki
 */

/*
 *------------------------
 * GLOBAL ENVIRONMENT
 *------------------------
 *
 * 每个操作会经过三层过滤:
 * (1) 系统层 - 入口脚本的同级目录 (root)
 * (2) 用户层 - /usr 目录, 用户自定义程序 (user)
 * (3) 安全层 - 模块目录, 对请求与部分指令做出判定, 并负责记录过滤日志 (log)
 *
 * 层级化, 组件化, 多选择, 易重写, 关联继承
 * 外部请求 >>> 系统路由表 >>> 模块路由表 >>> 安全过滤池 >>> 连接层 >>> 主题 >>> 符号过滤 >>> 输出
 */


# [1] 初始化系统, 鉴别 php 版本
$system = new sys();
$system->init('etc/local.xml');

$api = $system->route(true);
if ($system->useRoute)
{
    $system->linkToUsr($api);
}
