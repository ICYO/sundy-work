<?php
namespace bin;


class config
{
    public function __construct($xml)
    {
        $parse = new dataParse();  // 初始化 xml 解析工具
        $config = $parse->xml_object($xml);  // config 对象
        foreach ($config as $key => $val) {
            if (! $this->makeVal($key, $val)) {
                die("系统环境变量生成失败".__FILE__);
            }
        }
    }

    public function add($key, $val)
    {
        $this->makeVal($key, $val);
    }

    private function makeVal($key, $val)
    {
        $val = trim($val);
        $this->$key = $val;
        return isset($this->$key);
    }
}