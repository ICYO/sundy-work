<?php
namespace bin;
require_once('bin/dataParse.php');

class sys
{
    private $local;
    public $useRoute = true;

    public function __construct()
    {
        $this->checkVersion();  // 检测 php 版本
        $this->autoload();
    }

/**
 * @param localFile 系统 local 配置xml文件, 或xml格式的字符串
 */
    public function init($localFile)
    {
        $parse = new dataParse();  // 初始化 xml 解析工具
        $this->local = $parse->xml_object($localFile);  // local 对象
        $this->config = $this->makeConfig();  // config 对象
        $this->using = $this->makeUsing();  // 开启库调用功能
    }

    public function route($key)
    {
        if (! $key) {
            $this->useRoute = false;
            return false;
        }
        /* 调用路由 */
        $route = $this->using->lib('lib', 'sysRoute');
        $route->file = $this->local->sysRule;  // 传入系统路由表
        $route->urlInit = $this->config->script;  // 传入入口脚本
        /* 激活路由 */
        $route->init();  // 初始化
        return $this->makeApi($route->api);
    }

    public function makeUsing()
    {
        $using = new using();
        return $using;
    }

    public function linkToUsr($api)
    {
        require_once('usr/index.php');
    }

    private function makeApi($api)
    {
        $api['script'] = $this->config->script;
        return $api;
    }

/**
 * 自动加载
 */
    private function autoload()
    {
        spl_autoload_register(function ($class){
            if (! require_once("{$class}.php")) {
                die("没有找到这个类($class)");
            }
        });
    }

/**
 * 引入并生成 config 对象
 */
    private function makeConfig()
    {
        $env = $this->local->env;
        return new config($env);
    }

/**
 * 加载组件包(component)中的函数读入到系统函数
 */
    public function __call($func, $arg)
    {
        include_once("bin/component/$func.php");
        call_user_func_array($func, $arg);
    }

/**
 * 检测 php 版本
 */
    private function checkVersion()
    {
        if (version_compare(PHP_VERSION, '5.3', '>=') !== true) {
            header('HTTP/1.1 500', TRUE, 500);
            echo "php version error: >= 5.3 .";
        }
    }
}