<?php
namespace bin;


class dataParse
{
    public function xml_object($xml)
    {
        $xml = trim($xml);
        if (file_exists($xml)) {
            $swap = simplexml_load_file($xml);  // 读取 xml 配置文件转化为 object
        }
        else {
            $swap = simplexml_load_string($xml);
        }
        unset($swap->comment);
        return $swap;
    }
}