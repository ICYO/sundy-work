<?php
namespace bin;


class using
{
    public function __construct()
    {
        global $system;
        $this->system = $system;
    }

    public function lib($space, $name)
    {
        $pack = $this->system->config->lib;
        $spath = preg_replace('/\\\/', '/', $space);
        $filePath = "$spath/$pack/component/$name.php";
        if (! file_exists($filePath)) {
            $filePath = "$space/org/component/$name.php";
        }
        require_once($filePath);
        $name = "\\$space\\$name";
        return new $name();
    }
}