<?php
$url = "http://127.0.0.1:8080/req.php";

$da = array(
    'name' => 'XiaoMing',
    'age'  => 20.0
);
$data = http_build_query($da);

$opt = array(
    'http' => array(
        'method' => 'POST',
        'header' => "Cookie: foo=bar; name=age\r\n".
                    "Content-type:application/x-www-form-urlencoded\r\n".
                    "Accept-Language: en\r\n".
                    "Cache-Control: no-cache\r\n".
                    "Accept-Encoding: gzip\r\n",
        'content' => $data
    )
);
$context = stream_context_create($opt);
echo readfile($url, false, $context);
echo json_encode($da, JSON_PRESERVE_ZERO_FRACTION);